﻿public static class ExtensionMethods
{
    public static float Remap (this float value, float fromMin, float fromMax, float toMin, float toMax)
    {
        float fromMinAbs = value - fromMin;
        float fromMaxAbs = fromMax - fromMin;

        float normal = fromMinAbs / fromMaxAbs;

        float toMaxAbs = toMax - toMin;
        float toMinAbs = toMaxAbs * normal;

        return toMinAbs + toMin;
    }
}