﻿// unset

using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Audio;

namespace DefaultNamespace
{
    public class MixerFaderBehaviour : MonoBehaviour
    {
        [SerializeField] private AudioMixer mixer;
        [SerializeField] private string exposedParam;
        [SerializeField] private float targetVolume = 1.0f;

        public HashSet<Transform> activeEnemies = new HashSet<Transform>(); 

        private void Update()
        {
            float targetValue = activeEnemies.Count > 0 ? targetVolume : 0.0f;

            print ($"number of enemies persuing: {activeEnemies.Count}");
            
            float currentValue;
            mixer.GetFloat (exposedParam, out currentValue);
            currentValue = Mathf.Pow (10.0f, currentValue / 20.0f);
            
            mixer.SetFloat (exposedParam, Mathf.Log10  (Mathf.Lerp (currentValue, targetValue, Time.deltaTime)) * 20.0f);
        }
    }
}